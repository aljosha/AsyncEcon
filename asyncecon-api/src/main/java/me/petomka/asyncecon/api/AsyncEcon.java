package me.petomka.asyncecon.api;

import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.UserAccount;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

public interface AsyncEcon extends Economy {

	static AsyncEcon getAsyncEcon() {
		Plugin plugin = Bukkit.getPluginManager().getPlugin("AsyncEcon");
		if(plugin == null) {
			return null;
		}
		return (AsyncEcon) plugin;
	}

	Logger getLogger();

	List<Bank> getAsyncEconBanks();

	UserAccount getUser(UUID playerId);

	UserAccount getConsoleAccount();

	IDatabaseCache getDatabaseCache();

}
