package me.petomka.asyncecon.api.event;

import lombok.Getter;
import lombok.Setter;
import me.petomka.asyncecon.api.economy.UserAccount;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@Setter
public class MoneySendEvent extends Event implements Cancellable {

	private static HandlerList handlerList = new HandlerList();

	UserAccount senderAccount;
	UserAccount receiverAccount;
	double amount;

	public MoneySendEvent(UserAccount senderAccount, UserAccount receiverAccount, double amount) {
		this.senderAccount = senderAccount;
		this.receiverAccount = receiverAccount;
		this.amount = amount;
	}

	private boolean cancelled;

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

	public static HandlerList getHandlerList() {
		return handlerList;
	}
}
