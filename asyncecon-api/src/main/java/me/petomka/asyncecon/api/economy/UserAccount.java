package me.petomka.asyncecon.api.economy;

import java.math.BigDecimal;
import java.util.UUID;

public interface UserAccount {

	UUID getPlayerId();

	BigDecimal getBalance();

	BigDecimal getBalanceIgnoreInfinite();

	void setBalance(BigDecimal balance);

	boolean isInfinite();

	void setInfinite(boolean infinite);

	boolean canAfford(double v);

	void deposit(double amount);

	void withdraw(double amount);

	boolean isOnline();

}
