package me.petomka.asyncecon.api.event;

import lombok.Getter;
import lombok.Setter;
import me.petomka.asyncecon.api.economy.UserAccount;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Setter
@Getter
public class MoneySetEvent extends Event implements Cancellable {

	private static HandlerList handlerList = new HandlerList();

	private CommandSender sender;
	private UserAccount targetAccount;
	private double amount;

	private boolean cancelled;

	public MoneySetEvent(CommandSender sender, UserAccount targetAccount, double amount) {
		this.sender = sender;
		this.targetAccount = targetAccount;
		this.amount = amount;
	}

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

	public static HandlerList getHandlerList() {
		return handlerList;
	}

}
