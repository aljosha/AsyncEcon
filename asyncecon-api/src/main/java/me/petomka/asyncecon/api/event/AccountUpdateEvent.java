package me.petomka.asyncecon.api.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import javax.annotation.Nonnull;
import java.util.UUID;

/**
 * Fired on the server that received the instruction to update a player's account.
 */
@RequiredArgsConstructor
public class AccountUpdateEvent extends Event implements Cancellable {

	@Getter
	private static HandlerList handlerList = new HandlerList();

	@Getter
	private final @Nonnull UUID playerUUID;

	private boolean cancelled = false;

	@Override
	public HandlerList getHandlers() {
		return AccountUpdateEvent.getHandlerList();
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
