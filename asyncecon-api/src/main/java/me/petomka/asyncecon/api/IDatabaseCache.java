package me.petomka.asyncecon.api;

import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.UserAccount;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

public interface IDatabaseCache {

	void loadBanks();

	void saveBanks();

	void saveBank(Bank bank);

	@Nullable String getBankName(int bankId);

	@Nullable Integer getBankId(@Nonnull String bankName);

	@Nullable Bank getBank(String bankName);

	@Nullable Bank getBank(int bankId);

	List<Bank> getBanks(@Nonnull UUID playerId);

	boolean hasBankAccount(@Nonnull UUID playerId, @Nonnull String bankName);

	boolean createBank(@Nonnull String name);

	boolean deleteBank(int bankId);

	void loadUser(@Nonnull UUID playerId);

	void unloadUser(@Nonnull UUID playerId);

	void unloadUser(@Nonnull UUID playerId, boolean save);

	@Nonnull UserAccount getUser(@Nonnull UUID playerId);

	void saveUser(@Nonnull UUID playerId);

	void saveUser(@Nonnull UserAccount account);

	void saveUsers();

	void saveAll();

}
