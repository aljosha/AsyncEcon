package me.petomka.asyncecon.api.economy;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.UUID;

public interface BankMember {

	@Nullable Integer getBankMemberId();

	UUID getPlayerId();

	String getBankName();

	boolean isBankOwner();

	void setBankOwner(boolean bankOwner);

	BigDecimal getBalance();

	void setBalance(BigDecimal newBalance);

}
