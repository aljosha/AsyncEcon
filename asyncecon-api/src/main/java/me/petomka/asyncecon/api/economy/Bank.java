package me.petomka.asyncecon.api.economy;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface Bank {

	String getName();

	int getId();

	List<BankMember> getMembers();

	BigDecimal getBalance();

	void setBalance(BankMember member, BigDecimal newBalance);

	void addMember(UUID playerId, boolean owner, BigDecimal balance);

	void addMember(BankMember bankMember);

	BankMember getMember(UUID playerID);

	BankMember removeMember(UUID playerID);

}
