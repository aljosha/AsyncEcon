package me.petomka.asyncecon.api.event;

import lombok.Getter;
import lombok.Setter;
import me.petomka.asyncecon.api.economy.UserAccount;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@Setter
public class AccountInfiniteStatusChangeEvent extends Event implements Cancellable {

	private static HandlerList handlerList = new HandlerList();

	private boolean cancelled;

	private CommandSender sender;
	private UserAccount targetAccount;
	private boolean infinite;

	public AccountInfiniteStatusChangeEvent(CommandSender sender, UserAccount targetAccount, boolean infinite) {
		this.sender = sender;
		this.targetAccount = targetAccount;
		this.infinite = infinite;
	}

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

	public static HandlerList getHandlerList() {
		return handlerList;
	}
}
