package me.petomka.asyncecon.core.database;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.petomka.asyncecon.api.IDatabaseCache;
import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.BankMember;
import me.petomka.asyncecon.api.economy.UserAccount;
import me.petomka.asyncecon.core.economy.DefaultUserAccount;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class DatabaseCache implements IDatabaseCache {

	@Getter
	private static DatabaseCache instance = new DatabaseCache();

	@Getter
	private List<Bank> loadedBanks = Lists.newCopyOnWriteArrayList();

	@Getter
	private List<UserAccount> loadedUserAccounts = Lists.newCopyOnWriteArrayList();

	public void loadBanks() {
		loadedBanks.addAll(DatabaseModel.getInstance().getBanks());
		loadedBanks.forEach(bank -> DatabaseModel.getInstance().getBankMembers(bank.getId()).forEach(bank::addMember));
	}

	public void saveBanks() {
		loadedBanks.forEach(this::saveBank);
	}

	public void saveBank(Bank bank) {
		DatabaseModel.getInstance().saveBank(bank);
	}

	public @Nullable
	String getBankName(int bankId) {
		return loadedBanks.stream()
				.filter(bank -> bank.getId() == bankId)
				.map(Bank::getName).findFirst()
				.orElse(null);
	}

	public @Nullable
	Integer getBankId(@Nonnull String bankName) {
		return loadedBanks.stream()
				.filter(bank -> bank.getName().equals(bankName))
				.map(Bank::getId).findFirst()
				.orElse(null);
	}

	public @Nullable
	Bank getBank(String bankName) {
		return loadedBanks.stream()
				.filter(bank -> bank.getName().equals(bankName))
				.findFirst()
				.orElse(null);
	}

	public @Nullable
	Bank getBank(int bankId) {
		return loadedBanks.stream()
				.filter(bank -> bank.getId() == bankId)
				.findFirst()
				.orElse(null);
	}

	public List<Bank> getBanks(@Nonnull UUID playerId) {
		return loadedBanks.stream()
				.filter(bank -> bank.getMembers().stream()
						.map(BankMember::getPlayerId)
						.anyMatch(playerId::equals))
				.collect(Collectors.toList());
	}

	public boolean hasBankAccount(@Nonnull UUID playerId, @Nonnull String bankName) {
		return loadedBanks.stream()
				.filter(bank -> bank.getName().equals(bankName))
				.findFirst()
				.map(bank -> bank.getMember(playerId) != null)
				.orElse(false);
	}

	public boolean createBank(@Nonnull String name) {
		if (getBankId(name) != null) {
			return false;
		}
		Bank bank = DatabaseModel.getInstance().createBank(name);
		if (bank == null) {
			return false;
		}
		loadedBanks.add(bank);
		return true;
	}

	public boolean deleteBank(int bankId) {
		if (getBankName(bankId) == null) {
			return false;
		}
		loadedBanks.remove(bankId);
		DatabaseModel.getInstance().removeBank(bankId);
		return true;
	}

	public void loadUser(@Nonnull UUID playerId) {
		UserAccount account = DatabaseModel.getInstance().getUserAccount(playerId);
		if (account == null) {
			getUser(playerId);
		} else {
			loadedUserAccounts.add(account);
		}
	}

	public void unloadUser(@Nonnull UUID playerId) {
		unloadUser(playerId, true);
	}

	public void unloadUser(@Nonnull UUID playerId, boolean save) {
		UserAccount account = getUser(playerId);
		if (save) {
			saveUser(account);
		}
		loadedUserAccounts.remove(account);
	}

	public @Nonnull
	UserAccount getUser(@Nonnull UUID playerId) {
		return loadedUserAccounts.stream()
				.filter(account1 -> account1.getPlayerId().equals(playerId))
				.findFirst().orElseGet(() -> {
					UserAccount newAccount = Optional.ofNullable(DatabaseModel.getInstance().getUserAccount(playerId))
							.orElse(DefaultUserAccount.getNewAccount(playerId));
					loadedUserAccounts.add(newAccount);
					return newAccount;
				});
	}

	public void saveUser(@Nonnull UUID playerId) {
		DatabaseModel.getInstance().updateSaveUser(getUser(playerId));
	}

	public void saveUser(@Nonnull UserAccount account) {
		DatabaseModel.getInstance().updateSaveUser(account);
	}

	public void saveUsers() {
		loadedUserAccounts.forEach(this::saveUser);
	}

	public void saveAll() {
		saveBanks();
		saveUsers();
		loadedUserAccounts.clear();
	}

}
