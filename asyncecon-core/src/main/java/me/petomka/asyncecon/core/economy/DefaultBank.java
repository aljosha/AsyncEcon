package me.petomka.asyncecon.core.economy;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.BankMember;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public class DefaultBank implements Bank {

	@Getter
	private final String name;

	@Getter
	private final int id;

	private List<BankMember> bankMembers = Lists.newCopyOnWriteArrayList();

	@Override
	public List<BankMember> getMembers() {
		return Lists.newArrayList(bankMembers);
	}

	@Override
	public BigDecimal getBalance() {
		BigDecimal balance = new BigDecimal(0);
		for(BankMember bankMember : bankMembers) {
			balance = balance.add(bankMember.getBalance());
		}
		return balance;
	}

	@Override
	public void setBalance(BankMember member, BigDecimal newBalance) {
		if(!bankMembers.contains(member)) {
			return;
		}
		member.setBalance(newBalance);
	}

	@Override
	public void addMember(UUID playerId, boolean owner, BigDecimal balance) {
		addMember(new DefaultBankMember(playerId, this.name, null, owner, balance));
	}

	@Override
	public void addMember(BankMember bankMember) {
		bankMembers.add(bankMember);
	}

	@Override
	public BankMember getMember(UUID playerID) {
		return bankMembers.stream()
				.filter(bankMember -> bankMember.getPlayerId().equals(playerID))
				.findFirst().orElse(null);
	}

	@Override
	public BankMember removeMember(UUID playerID) {
		BankMember toRemove = getMember(playerID);
		if(toRemove == null) {
			return null;
		}
		bankMembers.remove(toRemove);
		return toRemove;
	}
}
