package me.petomka.asyncecon.core.communication.packets;


import lombok.Data;
import lombok.RequiredArgsConstructor;
import me.petomka.asyncecon.core.communication.ByteArrayUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.UUID;

@RequiredArgsConstructor
@Data
public class AEPacket implements Serializable {

	/**
	 * To later recognize the packet again
	 */
	final UUID packetUUID = UUID.randomUUID();

	/**
	 * The player the plugin message was originally sent with
	 */
	final @Nonnull UUID senderUUID;

	/**
	 * The player that this packet was created for.
	 */
	final @Nullable UUID receiverUUID;

	private boolean handled = false;

	public byte[] toByteArray() {
		return ByteArrayUtils.toByteArray(this);
	}

}
