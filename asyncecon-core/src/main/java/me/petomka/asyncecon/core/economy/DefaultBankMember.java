package me.petomka.asyncecon.core.economy;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import me.petomka.asyncecon.api.economy.BankMember;

import java.math.BigDecimal;
import java.util.UUID;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = {"playerId", "bankName"})
public class DefaultBankMember implements BankMember {

	private final UUID playerId;
	private final String bankName;
	private final Integer bankMemberId;

	@Setter
	private boolean bankOwner;
	@Setter
	private BigDecimal balance;

}
