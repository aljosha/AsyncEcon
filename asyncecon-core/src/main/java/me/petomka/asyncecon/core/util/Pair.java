package me.petomka.asyncecon.core.util;

import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

@Data
@AllArgsConstructor
public class Pair<L, R> {

	public static <L, R> Pair<L, R> of(L left, R right) {
		return new Pair<>(left, right);
	}

	L left;
	R right;

	public L getKey() {
		return left;
	}

	public R getValue() {
		return right;
	}

	public void setKey(L key) {
		this.left = key;
	}

	public void setValue(R value) {
		this.right = value;
	}

	public static <L, R> Collector<Pair<L, R>, ?, Map<L, R>> pairToMap() {
		return new PairToMapCollector();
	}

	public static class PairToMapCollector<K, V> implements Collector<Pair<K, V>, ImmutableMap.Builder<K, V>, ImmutableMap<K, V>> {
		@Override
		public Supplier<ImmutableMap.Builder<K, V>> supplier() {
			return ImmutableMap::builder;
		}

		@Override
		public BiConsumer<ImmutableMap.Builder<K, V>, Pair<K, V>> accumulator() {
			return (kvBuilder, kvPair) -> kvBuilder.put(kvPair.left, kvPair.right);
		}

		@Override
		public BinaryOperator<ImmutableMap.Builder<K, V>> combiner() {
			return (kvBuilder, kvBuilder2) -> {
				kvBuilder.putAll(kvBuilder2.build());
				return kvBuilder;
			};
		}

		@Override
		public Function<ImmutableMap.Builder<K, V>, ImmutableMap<K, V>> finisher() {
			return ImmutableMap.Builder::build;
		}

		@Override
		public Set<Characteristics> characteristics() {
			return EnumSet.of(Characteristics.UNORDERED);
		}
	}

}
