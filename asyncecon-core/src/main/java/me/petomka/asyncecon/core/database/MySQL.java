package me.petomka.asyncecon.core.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.experimental.UtilityClass;
import me.petomka.asyncecon.core.AsyncEconCoreMain;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

@UtilityClass
public class MySQL {

	private static HikariDataSource ds;

	public static void init() {
		AsyncEconCoreMain econ = AsyncEconCoreMain.getInstance();

		HikariConfig config = new HikariConfig();
		config.setJdbcUrl("jdbc:mysql://" + econ.getDbHost() + ":" + econ.getDbPort() + "/" + econ.getDbDatabasename() + "?useSSL=" + econ.isDbSSL());
		config.setUsername(econ.getDbUsername());
		config.setPassword(econ.getDbPassword());
		config.setPoolName("AsyncEcon-" + UUID.randomUUID().toString().substring(0, 6));
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

		ds = new HikariDataSource(config);
	}

	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	public static void closeDatasource() {
		ds.close();
	}

}
