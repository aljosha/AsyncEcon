package me.petomka.asyncecon.core.util;

import lombok.Getter;
import lombok.Setter;
import me.petomka.asyncecon.api.AsyncEcon;
import me.petomka.asyncecon.api.economy.UserAccount;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.math.BigDecimal;

public class ConfigUtils {

	@Setter
	@Getter
	private static FileConfiguration config;

	private static AsyncEcon econ = AsyncEcon.getAsyncEcon();

	public static String getPrefix() {
		return ChatColor.translateAlternateColorCodes('&', config.getString("message.prefix"));
	}

	public static String getString(String path) {
		return ChatColor.translateAlternateColorCodes('&', config.getString(path))
				.replace("<prefix>", getPrefix());
	}

	public static String getString(String path, UserAccount userAccount) {
		return getString(path).replace("<balance>", econ.format(userAccount.getBalance().doubleValue()));
	}

	public static String getString(String path, BigDecimal balance) {
		return getString(path).replace("<balance>", econ.format(balance.doubleValue()));
	}

}
