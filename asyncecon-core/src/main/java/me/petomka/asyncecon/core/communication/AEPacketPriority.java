package me.petomka.asyncecon.core.communication;

public enum AEPacketPriority {

	LOWEST,
	LOW,
	NORMAL,
	HIGH,
	HIGHEST,
	MONITOR

}
