package me.petomka.asyncecon.core.util;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.stream.Stream;

public class Menu {

	@Getter
	private static final String INDENT_SEQUENCE = "\u00BB";

	@Getter
	@Setter
	private ClickEvent.Action action = null;

	@Getter
	@Setter
	private String actionString = "";

	@Getter
	@Setter
	private String hoverString;

	@Getter
	@Setter
	private String message;

	private List<Menu> subMenus = Lists.newArrayList();

	public List<BaseComponent[]> toComponents() {
		return toComponents(message, 0);
	}

	public Menu(String message) {
		this.message = message;
	}

	public Menu(String message, String hoverString) {
		this(message);
		this.hoverString = hoverString;
	}

	public Menu(String message, ClickEvent.Action action, String actionString) {
		this.message = message;
		this.action = action;
		this.actionString = actionString;
	}

	public Menu(String message, ClickEvent.Action action, String actionString, String hoverString) {
		this(message, action, actionString);
		this.hoverString = hoverString;
	}

	public List<BaseComponent[]> toComponents(String message, int indent) {
		List<BaseComponent[]> components = Lists.newArrayList();
		BaseComponent[] localComponents = TextComponent.fromLegacyText(message);
		if (action != null) {
			Stream.of(localComponents).forEach(component ->
					component.setClickEvent(new ClickEvent(action, actionString)));
		}
		if (hoverString != null) {
			Stream.of(localComponents).forEach(component ->
					component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
							TextComponent.fromLegacyText(hoverString))));
		}
		components.add(localComponents);
		subMenus.forEach(subMenu -> components.addAll(subMenu.toComponents(indent + 1)));
		return components;
	}

	public List<BaseComponent[]> toComponents(int indentation) {
		return toComponents(indentation(indentation).concat(message), indentation);
	}

	public Menu addSub(Menu menu) {
		if (menu.equals(this)) {
			return this;
		}
		subMenus.add(menu);
		return this;
	}

	public boolean removeSub(Menu menu) {
		return subMenus.remove(menu);
	}

	public void clearSubs() {
		subMenus.clear();
	}

	public void send(CommandSender sender) {
		toComponents().forEach(baseComponents -> sender.spigot().sendMessage(baseComponents));
	}

	public int getSubMenuCount() {
		return subMenus.size();
	}

	private String indentation(int ind) {
		StringBuilder stringBuilder = new StringBuilder(" ");
		while (ind-- > 1) {
			stringBuilder.append(" ");
		}
		stringBuilder.append(INDENT_SEQUENCE).append(" ");
		return stringBuilder.toString();
	}

}
