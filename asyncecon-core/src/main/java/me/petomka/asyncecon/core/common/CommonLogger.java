package me.petomka.asyncecon.core.common;

import lombok.Getter;
import lombok.Setter;

import java.util.logging.Logger;

public class CommonLogger {

	@Getter
	@Setter
	private static Logger logger;

}
