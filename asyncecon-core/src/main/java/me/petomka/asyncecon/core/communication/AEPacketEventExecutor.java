package me.petomka.asyncecon.core.communication;

import lombok.SneakyThrows;
import me.petomka.asyncecon.core.communication.packets.AEPacket;
import net.techcable.event4j.EventBus;
import net.techcable.event4j.EventExecutor;
import net.techcable.event4j.RegisteredListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

public class AEPacketEventExecutor<E, L> implements EventExecutor<E, L>, EventExecutor.Factory {
	private final Method method;
	public static final Factory PACKET_EVENT_LISTENER_FACTORY = AEPacketEventExecutor::new;

	public AEPacketEventExecutor(EventBus<E, L> eventBus, Method method) {
		RegisteredListener.validate(Objects.requireNonNull(eventBus, "Null eventBus"),
				Objects.requireNonNull(method, "Null method"));
		method.setAccessible(true);
		this.method = method;
	}

	@Override
	@SneakyThrows
	public void fire(L packetListener, E packet) {
		try {
			if(!(packet instanceof AEPacket)) {
				return;
			}
			if(method.getAnnotation(AEPacketEventHandler.class).ignoreHandled() &&
					((AEPacket) packet).isHandled()) {
				return;
			}
			method.invoke(packetListener, packet);
			((AEPacket) packet).setHandled(true);
		} catch (InvocationTargetException e) {
			throw e.getTargetException();
		}
	}

	@Override
	public <Event, Listener> EventExecutor<Event, Listener> create(EventBus<Event, Listener> eventBus, Method method) {
		return new AEPacketEventExecutor<>(eventBus, method);
	}
}
