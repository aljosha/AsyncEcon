package me.petomka.asyncecon.core.stuff;

import me.petomka.asyncecon.api.event.AccountUpdateEvent;
import me.petomka.asyncecon.core.communication.AEPacketEventHandler;
import me.petomka.asyncecon.core.communication.AEPacketListener;
import me.petomka.asyncecon.core.communication.packets.AEUpdatePlayerPacket;
import me.petomka.asyncecon.core.database.DatabaseCache;
import org.bukkit.Bukkit;

public class PacketListener implements AEPacketListener {

	@AEPacketEventHandler
	public void onAccountUdpate(AEUpdatePlayerPacket packet) {
		AccountUpdateEvent event = new AccountUpdateEvent(packet.getPlayerToUpdateUUID());
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled()) {
			return;
		}
		DatabaseCache.getInstance().unloadUser(packet.getPlayerToUpdateUUID(), false);
	}

}
