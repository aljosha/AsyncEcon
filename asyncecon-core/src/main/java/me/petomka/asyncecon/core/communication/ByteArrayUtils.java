package me.petomka.asyncecon.core.communication;

import me.petomka.asyncecon.core.common.CommonLogger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.logging.Level;

public class ByteArrayUtils {

	public static byte[] toByteArray(Object object) {
		try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream()) {
			try (ObjectOutputStream objOut = new ObjectOutputStream(byteOut)) {
				objOut.writeObject(object);
				objOut.flush();
				return byteOut.toByteArray();
			}
		} catch (IOException e) {
			CommonLogger.getLogger().log(Level.SEVERE,
					"Error serializing object to byte array! Object: " + object, e);
		}
		return null;
	}

	public static Object toObject(byte[] bytes) {
		try (ByteArrayInputStream byteIn = new ByteArrayInputStream(bytes)) {
			try (ObjectInputStream objIn = new ObjectInputStream(byteIn)) {
				return objIn.readObject();
			}
		} catch (IOException | ClassNotFoundException e) {
			CommonLogger.getLogger().log(Level.SEVERE, "Error " +
					"deserializing byte array!\nBytes: " + Arrays.toString(bytes), e);
		}
		return null;
	}

}
