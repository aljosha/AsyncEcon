package me.petomka.asyncecon.core.commands;

import com.google.common.collect.ImmutableList;
import me.petomka.asyncecon.api.AsyncEcon;
import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.UserAccount;
import me.petomka.asyncecon.api.event.AccountInfiniteStatusChangeEvent;
import me.petomka.asyncecon.api.event.MoneyGiveEvent;
import me.petomka.asyncecon.api.event.MoneySendEvent;
import me.petomka.asyncecon.api.event.MoneySetEvent;
import me.petomka.asyncecon.api.event.MoneyTakeEvent;
import me.petomka.asyncecon.core.AsyncEconCoreMain;
import me.petomka.asyncecon.core.common.Permissions;
import me.petomka.asyncecon.core.database.DatabaseCache;
import me.petomka.asyncecon.core.database.DatabaseModel;
import me.petomka.asyncecon.core.economy.DefaultUserAccount;
import me.petomka.asyncecon.core.util.ConfigUtils;
import me.petomka.asyncecon.core.util.Menu;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class AsyncEconCommand implements CommandExecutor, TabExecutor {

	private AsyncEcon econ = AsyncEcon.getAsyncEcon();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		UserAccount account;
		if (sender instanceof Player) {
			account = econ.getUser(((Player) sender).getUniqueId());
		} else {
			account = econ.getConsoleAccount();
		}

		if (args.length == 0) {
			if (sender instanceof Player) {
				onAccountOverviewCommand((Player) sender, account);
			} else {
				onAccountOverviewCommand(sender, account);
			}
			return true;
		}

		if (args.length == 2 && args[0].equalsIgnoreCase("info") && sender.hasPermission(Permissions.INFO_OTHER)) {
			OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
			if (player == null) {
				sender.sendMessage(ConfigUtils.getString("message.unknown-player"));
				return true;
			}
			if (sender instanceof Player) {
				onAccountOverviewOtherCommand((Player) sender, econ.getUser(player.getUniqueId()), player.getName());
			} else {
				onAccountOverviewOtherCommand(sender, econ.getUser(player.getUniqueId()), player.getName());
			}
			return true;
		}

		if (args.length == 3 && (args[0].equalsIgnoreCase("send") || args[0].equalsIgnoreCase("pay"))
				&& sender.hasPermission(Permissions.SEND_MONEY)) {
			Double amount = CommandUtils.getDouble(args[2]);
			if (amount == null || amount < 0.01) {
				sender.sendMessage(ConfigUtils.getString("message.no-number"));
				return true;
			}
			onMoneySend(sender, account, args[1], amount);
			return true;
		}

		if (args.length == 1 && args[0].equals("banks") && sender.hasPermission(Permissions.BANK_OVERVIEW)
				&& AsyncEcon.getAsyncEcon().hasBankSupport()) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(ConfigUtils.getString("message.players-only"));
				return true;
			}
			onBankOverview((Player) sender);
			return true;
		}

		if (args.length == 3 && args[0].equalsIgnoreCase("give") && sender.hasPermission(Permissions.GIVE_MONEY)) {
			Double amount = CommandUtils.getDouble(args[2]);
			if (amount == null || amount < 0.01) {
				sender.sendMessage(ConfigUtils.getString("message.no-number"));
				return true;
			}
			onMoneyGive(sender, args[1], amount);
			return true;
		}

		if (args.length == 3 && args[0].equalsIgnoreCase("take") && sender.hasPermission(Permissions.TAKE_MONEY)) {
			Double amount = CommandUtils.getDouble(args[2]);
			if (amount == null || amount < 0.01) {
				sender.sendMessage(ConfigUtils.getString("message.no-number"));
				return true;
			}
			onMoneyTake(sender, args[1], amount);
			return true;
		}

		if (args.length == 3 && args[0].equalsIgnoreCase("set") && sender.hasPermission(Permissions.SET_MONEY)) {
			Double amount = CommandUtils.getDouble(args[2]);
			if (amount == null) {
				sender.sendMessage(ConfigUtils.getString("message.no-number"));
				return true;
			}
			onMoneySet(sender, args[1], amount);
			return true;
		}

		if (args[0].equalsIgnoreCase("infinite") && sender.hasPermission(Permissions.ACCOUNT_INFINITE)) {
			if (args.length >= 2) {
				OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
				if (target == null) {
					sender.sendMessage(ConfigUtils.getString("message.unknown-player"));
					return true;
				}
				account = DatabaseCache.getInstance().getUser(target.getUniqueId());
			}
			boolean onOff = !account.isInfinite();
			if (args.length == 3) {
				onOff = CommandUtils.getBoolean(args[2]);
			}
			onAccountInfinite(sender, account, onOff);
			return true;
		}

		if (args.length == 3 && args[0].equalsIgnoreCase("db") && args[1].equalsIgnoreCase("upload") &&
				sender.hasPermission(Permissions.DB_UPLOAD)) {
			if (!onDBUpload(sender, args[2])) {
				sender.sendMessage(ConfigUtils.getString("message.db.illegal-selector"));
			}
			return true;
		}

		if (args.length == 3 && args[0].equalsIgnoreCase("db") && args[1].equalsIgnoreCase("download") &&
				sender.hasPermission(Permissions.DB_DOWNLOAD)) {
			if (!onDBDownload(sender, args[2])) {
				sender.sendMessage(ConfigUtils.getString("message.db.illegal-selector"));
			}
			return true;
		}

		if (args.length == 1 && args[0].equalsIgnoreCase("reload") && sender.hasPermission(Permissions.RELOAD_CONFIG)) {
			onReload(sender);
			return true;
		}

		if (sender instanceof Player) {
			onAccountOverviewCommand((Player) sender, account);
		} else {
			onAccountOverviewCommand(sender, account);
		}

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String label, String[] args) {
		String arg = args[args.length - 1];

		if (args.length == 1) {
			return CommandUtils.copyMatches(arg, ImmutableList.of("send", "banks", "info", "give", "take", "set", "infinite", "db", "reload"));
		}

		if (args.length == 2 && args[0].equalsIgnoreCase("db")) {
			return CommandUtils.copyMatches(arg, ImmutableList.of("download", "upload"));
		}

		if (args.length == 3 && args[0].equalsIgnoreCase("db") && (args[1].equalsIgnoreCase("download") || args[1].equalsIgnoreCase("upload"))) {
			if (arg.startsWith("@")) {
				arg = arg.substring(1);
				return CommandUtils.copyMatches(arg, econ.getBanks()).stream()
						.map(s -> "@" + s)
						.collect(Collectors.toList());
			}
		}

		return null;
	}

	private void onAccountOverviewCommand(Player sender, UserAccount account) {
		Menu overview = new Menu(ConfigUtils.getString("message.account.head"));
		if (sender.hasPermission(Permissions.BALANCE_SEE_OWN)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.balance", account.getBalance()),
							ConfigUtils.getString("message.account.balance-hover"))
			);
		}
		if (sender.hasPermission(Permissions.BALANCE_SEE_INFINITE)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.infinite-" + account.isInfinite()),
							ConfigUtils.getString("message.account.infinite-" + account.isInfinite() + "-hover"))
			);
			if (account.isInfinite()) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.balance-real", account.getBalanceIgnoreInfinite()),
								ConfigUtils.getString("message.account.balance-real-hover"))
				);
			}
		}
		if (sender.hasPermission(Permissions.SEND_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.send"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.send-click"),
							ConfigUtils.getString("message.account.send-hover"))
			);
		}
		if (sender.hasPermission(Permissions.BANK_OVERVIEW) && AsyncEcon.getAsyncEcon().hasBankSupport()) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.bank"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.bank-click"),
							ConfigUtils.getString("message.account.bank-hover"))
			);
		}
		if (sender.hasPermission(Permissions.INFO_OTHER)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.info"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.info-click"),
							ConfigUtils.getString("message.account.info-hover"))
			);
		}
		if (sender.hasPermission(Permissions.GIVE_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.give"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.give-click"),
							ConfigUtils.getString("message.account.give-hover"))
			);
		}
		if (sender.hasPermission(Permissions.TAKE_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.take"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.take-click"),
							ConfigUtils.getString("message.account.take-hover"))
			);
		}
		if (sender.hasPermission(Permissions.SET_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.set"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.set-click"),
							ConfigUtils.getString("message.account.set-hover"))
			);
		}
		if (sender.hasPermission(Permissions.ACCOUNT_INFINITE)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.infinite"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.infinite-click"),
							ConfigUtils.getString("message.account.infinite-hover"))
			);
		}
		if (sender.hasPermission(Permissions.DB_DOWNLOAD)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.db-download"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.db-download-click"),
							ConfigUtils.getString("message.account.db-download-hover"))
			);
		}
		if (sender.hasPermission(Permissions.DB_UPLOAD)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.db-upload"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.db-upload-click"),
							ConfigUtils.getString("message.account.db-upload-hover"))
			);
		}
		if (sender.hasPermission(Permissions.RELOAD_CONFIG)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.reload-config"), ClickEvent.Action.SUGGEST_COMMAND,
							ConfigUtils.getString("message.account.reload-config-click"),
							ConfigUtils.getString("message.account.reload-config-hover"))
			);
		}
		if (overview.getSubMenuCount() == 0) {
			overview.addSub(new Menu(ConfigUtils.getString("message.no-permissions")));
		}
		overview.send(sender);
	}

	private void onAccountOverviewCommand(CommandSender sender, UserAccount account) {
		Menu overview = new Menu(ConfigUtils.getString("message.account.head"));
		if (sender.hasPermission(Permissions.BALANCE_SEE_OWN)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.balance", account.getBalance()))
			);
		}
		if (sender.hasPermission(Permissions.BALANCE_SEE_INFINITE)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.infinite-" + account.isInfinite()))
			);
			if (account.isInfinite()) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.balance-real", account.getBalanceIgnoreInfinite()))
				);
			}
		}
		if (sender.hasPermission(Permissions.SEND_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.send-hover"))
			);
		}
		if (sender.hasPermission(Permissions.BANK_OVERVIEW)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.bank-hover"))
			);
		}
		if (sender.hasPermission(Permissions.INFO_OTHER)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.info-hover"))
			);
		}
		if (sender.hasPermission(Permissions.GIVE_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.give-hover"))
			);
		}
		if (sender.hasPermission(Permissions.TAKE_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.take-hover"))
			);
		}
		if (sender.hasPermission(Permissions.SET_MONEY)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.set-hover"))
			);
		}
		if (sender.hasPermission(Permissions.ACCOUNT_INFINITE)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.infinite-hover"))
			);
		}
		if (sender.hasPermission(Permissions.DB_DOWNLOAD)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.db-download-hover"))
			);
		}
		if (sender.hasPermission(Permissions.DB_UPLOAD)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.db-upload-hover"))
			);
		}
		if (sender.hasPermission(Permissions.RELOAD_CONFIG)) {
			overview.addSub(
					new Menu(ConfigUtils.getString("message.account.reload-config-hover"))
			);
		}
		if (overview.getSubMenuCount() == 0) {
			overview.addSub(new Menu(ConfigUtils.getString("message.no-permissions")));
		}
		overview.send(sender);
	}

	private void onAccountOverviewOtherCommand(Player sender, UserAccount other, String otherName) {
		if (sender.getName().equals(otherName)) {
			onAccountOverviewCommand(sender, other);
			return;
		}
		AsyncEconCoreMain.async(() -> {
			boolean infiniteShow = sender.hasPermission(Permissions.BALANCE_SEE_INFINITE);
			Menu overview = new Menu(ConfigUtils.getString("message.account-other.head").replace("<player>", otherName));
			if (sender.hasPermission(Permissions.BALANCE_SEE_OTHER)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.balance", infiniteShow ? other.getBalance() : other.getBalanceIgnoreInfinite()),
								ConfigUtils.getString("message.account.balance-hover")
										.replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.BALANCE_SEE_INFINITE)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.infinite-" + other.isInfinite()),
								ConfigUtils.getString("message.account.infinite-" + other.isInfinite() + "-hover"))
				);
				if (other.isInfinite()) {
					overview.addSub(
							new Menu(ConfigUtils.getString("message.account.balance-real", other.getBalanceIgnoreInfinite()),
									ConfigUtils.getString("message.account.balance-real-hover"))
					);
				}
			}
			if (sender.hasPermission(Permissions.SEND_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account-other.send").replace("<player>", otherName), ClickEvent.Action.SUGGEST_COMMAND,
								ConfigUtils.getString("message.account.send-click").concat(otherName).concat(" "),
								ConfigUtils.getString("message.account.send-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.GIVE_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account-other.give").replace("<player>", otherName), ClickEvent.Action.SUGGEST_COMMAND,
								ConfigUtils.getString("message.account.give-click").concat(otherName).concat(" "),
								ConfigUtils.getString("message.account.give-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.TAKE_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account-other.take").replace("<player>", otherName), ClickEvent.Action.SUGGEST_COMMAND,
								ConfigUtils.getString("message.account.take-click").concat(otherName).concat(" "),
								ConfigUtils.getString("message.account.take-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.SET_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account-other.set").replace("<player>", otherName), ClickEvent.Action.SUGGEST_COMMAND,
								ConfigUtils.getString("message.account.set-click").concat(otherName).concat(" "),
								ConfigUtils.getString("message.account.set-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.ACCOUNT_INFINITE)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account-other.infinite").replace("<player>", otherName), ClickEvent.Action.SUGGEST_COMMAND,
								ConfigUtils.getString("message.account.infinite-click").concat(otherName).concat(" "),
								ConfigUtils.getString("message.account.infinite-hover").replace("<player>", otherName))
				);
			}
			if (overview.getSubMenuCount() == 0) {
				overview.addSub(new Menu(ConfigUtils.getString("message.no-permissions")));
			}
			overview.send(sender);
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(other.getPlayerId());
		});
	}

	private void onAccountOverviewOtherCommand(CommandSender sender, UserAccount other, String otherName) {
		AsyncEconCoreMain.async(() -> {
			Menu overview = new Menu(ConfigUtils.getString("message.account.head"));
			if (sender.hasPermission(Permissions.BALANCE_SEE_OTHER)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.balance", other.getBalance())
								.replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.BALANCE_SEE_INFINITE)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.infinite-" + other.isInfinite()))
				);
				if (other.isInfinite()) {
					overview.addSub(
							new Menu(ConfigUtils.getString("message.account.balance-real", other.getBalanceIgnoreInfinite()))
					);
				}
			}
			if (sender.hasPermission(Permissions.SEND_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.send-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.GIVE_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.give-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.TAKE_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.take-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.SET_MONEY)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.set-hover").replace("<player>", otherName))
				);
			}
			if (sender.hasPermission(Permissions.ACCOUNT_INFINITE)) {
				overview.addSub(
						new Menu(ConfigUtils.getString("message.account.infinite-hover").replace("<player>", otherName))
				);
			}
			if (overview.getSubMenuCount() == 0) {
				overview.addSub(new Menu(ConfigUtils.getString("message.no-permissions")));
			}
			overview.send(sender);
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(other.getPlayerId());
		});
	}

	private void onMoneySend(CommandSender sender, UserAccount senderAccount, String receiverName, double amount) {
		OfflinePlayer receiver = Bukkit.getOfflinePlayer(receiverName);
		if (receiver == null) {
			sender.sendMessage(ConfigUtils.getString("message.unknown-player"));
			return;
		}
		AsyncEconCoreMain.async(() -> {
			if (!senderAccount.canAfford(amount)) {
				sender.sendMessage(ConfigUtils.getString("message.cant-afford"));
				return;
			}
			UserAccount receiverAccount = DatabaseCache.getInstance().getUser(receiver.getUniqueId());
			if (senderAccount.getPlayerId().equals(receiverAccount.getPlayerId())) {
				sender.sendMessage(ConfigUtils.getString("message.send.self-sent"));
				return;
			}
			MoneySendEvent event = new MoneySendEvent(senderAccount, receiverAccount, amount);
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled()) {
				return;
			}
			event.getSenderAccount().withdraw(event.getAmount());
			event.getReceiverAccount().deposit(event.getAmount());
			sender.sendMessage(ConfigUtils.getString("message.send.sent", new BigDecimal(event.getAmount()))
					.replace("<receiver>", receiver.getName()));
			if (receiver.isOnline()) {
				Player player = Bukkit.getPlayer(receiverName);
				player.sendMessage(ConfigUtils.getString("message.send.received", new BigDecimal(event.getAmount()))
						.replace("<sender>", sender.getName()));
			}
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(senderAccount.getPlayerId());
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(receiver.getUniqueId());
		});
	}

	private void onBankOverview(Player sender) {
		List<Bank> banks = DatabaseCache.getInstance().getBanks(sender.getUniqueId());
		Menu menu = new Menu(ConfigUtils.getString("message.banks.head"));
		banks.forEach(bank ->
				menu.addSub(
						new Menu(ConfigUtils.getString("message.banks.bank",
								bank.getMember(sender.getUniqueId()).getBalance())
								.replace("<bankname>", bank.getName()), ClickEvent.Action.SUGGEST_COMMAND,
								ConfigUtils.getString("message.banks.bank-click").replace("<bankname>", bank.getName()),
								ConfigUtils.getString("message.banks.bank-hover").replace("<bankname>", bank.getName()))
				));
		if (menu.getSubMenuCount() == 0) {
			menu.addSub(
					new Menu(ConfigUtils.getString("message.banks.no-banks"))
			);
		}
		menu.send(sender);
	}

	private void onMoneyGive(CommandSender sender, String receiverName, double amount) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(receiverName);
		if (player == null) {
			sender.sendMessage(ConfigUtils.getString("message.unknown-player"));
			return;
		}
		UserAccount targetAccount = DatabaseCache.getInstance().getUser(player.getUniqueId());
		MoneyGiveEvent event = new MoneyGiveEvent(sender, targetAccount, amount);
		Bukkit.getPluginManager().callEvent(event);
		AsyncEconCoreMain.async(() -> {
			if (event.isCancelled()) {
				return;
			}
			event.getTargetAccount().deposit(event.getAmount());
			event.getSender().sendMessage(ConfigUtils.getString("message.give.given",
					new BigDecimal(event.getAmount())).replace("<receiver>", player.getName()));
			if (player.isOnline()) {
				Player online = Bukkit.getPlayer(player.getUniqueId());
				online.sendMessage(ConfigUtils.getString("message.give.received", new BigDecimal(event.getAmount()))
						.replace("<sender>", event.getSender().getName()));
			}
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(targetAccount.getPlayerId());
		});
	}

	private void onMoneyTake(CommandSender sender, String receiverName, double amount) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(receiverName);
		if (player == null) {
			sender.sendMessage(ConfigUtils.getString("message.unknown-player"));
			return;
		}
		UserAccount targetAccount = DatabaseCache.getInstance().getUser(player.getUniqueId());
		MoneyTakeEvent event = new MoneyTakeEvent(sender, targetAccount, amount);
		Bukkit.getPluginManager().callEvent(event);
		AsyncEconCoreMain.async(() -> {
			if (event.isCancelled()) {
				return;
			}
			event.getTargetAccount().withdraw(event.getAmount());
			event.getSender().sendMessage(ConfigUtils.getString("message.take.taken",
					new BigDecimal(event.getAmount())).replace("<player>", player.getName()));
			if (player.isOnline()) {
				Player online = Bukkit.getPlayer(player.getUniqueId());
				online.sendMessage(ConfigUtils.getString("message.take.withdrawn", new BigDecimal(event.getAmount()))
						.replace("<player>", event.getSender().getName()));
			}
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(targetAccount.getPlayerId());
		});
	}

	private void onMoneySet(CommandSender sender, String receiverName, double amount) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(receiverName);
		if (player == null) {
			sender.sendMessage(ConfigUtils.getString("message.unknown-player"));
			return;
		}
		UserAccount targetAccount = DatabaseCache.getInstance().getUser(player.getUniqueId());
		MoneySetEvent event = new MoneySetEvent(sender, targetAccount, amount);
		Bukkit.getPluginManager().callEvent(event);
		AsyncEconCoreMain.async(() -> {
			if (event.isCancelled()) {
				return;
			}
			event.getTargetAccount().setBalance(new BigDecimal(event.getAmount()));
			event.getSender().sendMessage(ConfigUtils.getString("message.set.set-other",
					new BigDecimal(event.getAmount())).replace("<player>", player.getName()));
			if (player.isOnline()) {
				Player online = Bukkit.getPlayer(player.getUniqueId());
				online.sendMessage(ConfigUtils.getString("message.set.got-set", new BigDecimal(event.getAmount()))
						.replace("<player>", event.getSender().getName()));
			}
			AsyncEconCoreMain.updatePlayerOnBungeeSupport(targetAccount.getPlayerId());
		});
	}

	private void onAccountInfinite(CommandSender sender, UserAccount account, boolean infinite) {
		AccountInfiniteStatusChangeEvent event = new AccountInfiniteStatusChangeEvent(sender, account, infinite);
		Bukkit.getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return;
		}
		sender = event.getSender();
		account = event.getTargetAccount();
		infinite = event.isInfinite();
		account.setInfinite(infinite);
		String message;
		String senderMsg;
		if (infinite) {
			message = ConfigUtils.getString("message.infinite.now-infinite");
			senderMsg = ConfigUtils.getString("message.infinite.set-infinite");
		} else {
			message = ConfigUtils.getString("message.infinite.no-longer-infinite");
			senderMsg = ConfigUtils.getString("message.infinite.set-finite");
		}
		if (account.equals(DefaultUserAccount.getConsoleAccount())) {
			sender.sendMessage(message);
		} else {
			OfflinePlayer oPlayer = Bukkit.getOfflinePlayer(account.getPlayerId());
			sender.sendMessage(senderMsg.replace("<player>", oPlayer.getName()));
			if (oPlayer.isOnline()) {
				Player player = Bukkit.getPlayer(oPlayer.getUniqueId());
				player.sendMessage(message);
			}
		}
	}

	private boolean onDBUpload(CommandSender sender, String selector) {
		if (selector.startsWith("@")) {
			selector = selector.substring(1);
			if (selector.isEmpty()) {
				return false;
			}
			if (selector.equals("*")) {
				DatabaseCache.getInstance().saveBanks();
				sender.sendMessage(ConfigUtils.getString("message.db.upload.all-banks"));
				return true;
			} else {
				Bank bank = DatabaseCache.getInstance().getBank(selector);
				if (bank == null) {
					return false;
				}
				DatabaseCache.getInstance().saveBank(bank);
				sender.sendMessage(ConfigUtils.getString("message.db.upload.bank").replace("<bankname>",
						bank.getName()));
				return true;
			}
		} else {
			if (selector.equals("*")) {
				DatabaseCache.getInstance().saveUsers();
				sender.sendMessage(ConfigUtils.getString("message.db.upload.all-users"));
				return true;
			} else {
				OfflinePlayer player = Bukkit.getPlayer(selector);
				if (player == null) {
					return false;
				}
				DatabaseCache.getInstance().saveUser(player.getUniqueId());
				sender.sendMessage(ConfigUtils.getString("message.db.upload.user").replace("<player>",
						player.getName()));
				return true;
			}
		}
	}

	private boolean onDBDownload(CommandSender sender, String selector) {
		if (selector.startsWith("@")) {
			selector = selector.substring(1);
			if (selector.isEmpty()) {
				return false;
			}
			if (selector.equals("*")) {
				DatabaseCache.getInstance().getLoadedBanks().clear();
				DatabaseCache.getInstance().loadBanks();
				sender.sendMessage(ConfigUtils.getString("message.db.download.all-banks"));
				return true;
			} else {
				Bank bank = DatabaseCache.getInstance().getBank(selector);
				if (bank == null) {
					return false;
				}
				bank.getMembers().clear();
				DatabaseModel.getInstance().getBankMembers(bank.getId()).forEach(bank::addMember);
				sender.sendMessage(ConfigUtils.getString("message.db.download.bank").replace("<bankname>",
						bank.getName()));
				return true;
			}
		} else {
			if (selector.equals("*")) {
				DatabaseCache.getInstance().getLoadedUserAccounts().clear();
				sender.sendMessage(ConfigUtils.getString("message.db.download.all-users"));
				return true;
			} else {
				OfflinePlayer player = Bukkit.getPlayer(selector);
				if (player == null) {
					return false;
				}
				DatabaseCache.getInstance().getLoadedUserAccounts().remove(DatabaseCache.getInstance().getUser(player.getUniqueId()));
				sender.sendMessage(ConfigUtils.getString("message.db.download.user").replace("<player>",
						player.getName()));
				return true;
			}
		}
	}

	private void onReload(CommandSender sender) {
		AsyncEconCoreMain.getInstance().reloadConfig();
		sender.sendMessage(ConfigUtils.getString("message.config.reloaded"));
	}

}
