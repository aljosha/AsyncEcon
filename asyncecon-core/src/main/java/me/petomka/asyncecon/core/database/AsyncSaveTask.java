package me.petomka.asyncecon.core.database;

import me.petomka.asyncecon.api.AsyncEcon;
import me.petomka.asyncecon.core.AsyncEconCoreMain;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.logging.Level;

public class AsyncSaveTask extends BukkitRunnable {


	public AsyncSaveTask(int interval, Plugin plugin) {
		this.runTaskTimerAsynchronously(plugin, interval, interval);
	}


	@Override
	public void run() {
		boolean dologging = AsyncEconCoreMain.getInstance().getConfig().getBoolean("general.do-logging");
		if(dologging){
			AsyncEcon.getAsyncEcon().getLogger().log(Level.INFO, "Saving economy...");
		}
		DatabaseCache.getInstance().saveAll();
		if(dologging){
			AsyncEcon.getAsyncEcon().getLogger().log(Level.INFO, "Economy saved");
		}
	}

}
