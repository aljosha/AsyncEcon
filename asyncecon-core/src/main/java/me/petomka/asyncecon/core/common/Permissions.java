package me.petomka.asyncecon.core.common;

public class Permissions {

	//permission to see his own balance
	public static final String BALANCE_SEE_OWN = "asyncecon.balance";

	//permission to be able to see infinite money flags
	public static final String BALANCE_SEE_INFINITE = "asyncecon.infinite.see";

	//permission to send money to someone else
	public static final String SEND_MONEY = "asyncecon.send";

	//permission to lookup other bank accounts
	public static final String INFO_OTHER = "asyncecon.info";

	//permission to give money out of nothing
	public static final String GIVE_MONEY = "asyncecon.give";

	//permission to take money from any account
	public static final String TAKE_MONEY = "asyncecon.take";

	//permission to set an account to a value
	public static final String SET_MONEY = "asyncecon.set";

	//permission to see own bank overview
	public static final String BANK_OVERVIEW = "asyncecon.banks";

	//permission to set an account to infinite money
	public static final String ACCOUNT_INFINITE = "asyncecon.infinite";

	//permission to upload data to database
	public static final String DB_UPLOAD = "asyncecon.db.upload";

	//permission to download (potentially old) data from database
	public static final String DB_DOWNLOAD = "asyncecon.db.download";

	//permission to see another player's balance
	public static final String BALANCE_SEE_OTHER = "asyncecon.balance.others";

	//permission to see another player's bank overview
	public static final String BANK_OVERVIEW_OTHER = "asyncecon.banks.others";

	public static final String RELOAD_CONFIG = "asyncecon.reload";

}
