package me.petomka.asyncecon.core.database;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class SaveListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		DatabaseCache.getInstance().unloadUser(event.getPlayer().getUniqueId());
		DatabaseCache.getInstance().loadUser(event.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		DatabaseCache.getInstance().saveUser(event.getPlayer().getUniqueId());
		DatabaseCache.getInstance().unloadUser(event.getPlayer().getUniqueId());
	}

}
