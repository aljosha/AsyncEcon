package me.petomka.asyncecon.core.database;

import com.google.common.collect.Lists;
import lombok.Getter;
import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.BankMember;
import me.petomka.asyncecon.api.economy.UserAccount;
import me.petomka.asyncecon.core.AsyncEconCoreMain;
import me.petomka.asyncecon.core.economy.DefaultBank;
import me.petomka.asyncecon.core.economy.DefaultBankMember;
import me.petomka.asyncecon.core.economy.DefaultUserAccount;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseModel {

	@Getter
	private static DatabaseModel instance = new DatabaseModel();

	private Logger logger = AsyncEconCoreMain.getInstance().getLogger();

	private void onDBError(Throwable exception) {
		logger.log(Level.SEVERE, "Database error!", exception);
	}

	public void createTables() {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `ae_accounts` " +
					"(`uuid` VARCHAR(36) NOT NULL, `balance` DECIMAL(20,2) NOT NULL, `infinite` BOOLEAN NOT NULL, " +
					"PRIMARY KEY (`uuid`))")) {
				stmt.executeUpdate();
			}
			try (PreparedStatement stmt = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `ae_banks` " +
					"(`bank_id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(32) NOT NULL UNIQUE, PRIMARY KEY (`bank_id`))")) {
				stmt.executeUpdate();
			}
			try (PreparedStatement stmt = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `ae_bank_accounts` " +
					"(`id` INT NOT NULL AUTO_INCREMENT, `uuid` VARCHAR(36) NOT NULL, `bank_id` INT NOT NULL, " +
					"`owner` BOOLEAN NOT NULL, `balance` DECIMAL(20,2) NOT NULL, " +
					"PRIMARY KEY (`id`), FOREIGN KEY (`bank_id`) REFERENCES ae_banks(`bank_id`))")) {
				stmt.executeUpdate();
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
	}

	public @Nonnull
	List<Bank> getBanks() {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ae_banks")) {
				try (ResultSet resultSet = stmt.executeQuery()) {
					List<Bank> banks = Lists.newArrayList();
					while (resultSet.next()) {
						String bankName = SQLUtils.getString(resultSet, "name");
						Integer bankId = SQLUtils.getInt(resultSet, "bank_id");
						if (bankName != null && bankId != null) {
							banks.add(new DefaultBank(bankName, bankId));
						}
					}
					return banks;
				}
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
		return Collections.emptyList();
	}

	public @Nullable
	Bank createBank(@Nonnull String name) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO ae_banks (`bank_id`, `name`) VALUES (NULL , ?)")) {
				SQLUtils.setString(stmt, 1, name);
				stmt.executeUpdate();
			}
			Integer bankId = getBankId(name);
			if (bankId != null) {
				return new DefaultBank(name, bankId);
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
		return null;
	}

	public void removeBank(int id) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM ae_banks WHERE bank_id = ?")) {
				SQLUtils.setInt(stmt, 1, id);
				stmt.executeUpdate();
			}
			try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM ae_bank_accounts WHERE bank_id = ?")) {
				SQLUtils.setInt(stmt, 1, id);
				stmt.executeUpdate();
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
	}

	public @Nonnull
	List<BankMember> getBankMembers(int bankId) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM `ae_bank_accounts` WHERE bank_id = ?")) {
				SQLUtils.setInt(stmt, 1, bankId);
				List<BankMember> bankMembers = Lists.newArrayList();
				try (ResultSet resultSet = stmt.executeQuery()) {
					while (resultSet.next()) {
						bankMembers.add(new DefaultBankMember(
								SQLUtils.getUUID(resultSet, "uuid"),
								DatabaseCache.getInstance().getBankName(bankId),
								SQLUtils.getInt(resultSet, "id"),
								SQLUtils.getBoolean(resultSet, "owner"),
								SQLUtils.getBigDecimal(resultSet, "balance")
						));
					}
					return bankMembers;
				}
			}
		} catch (SQLException | NullPointerException exc) {
			onDBError(exc);
		}
		return Collections.emptyList();
	}

	public void saveBank(Bank bank) {
		try (Connection connection = MySQL.getConnection()) {
			for(BankMember member : bank.getMembers()) {
				try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO ae_bank_accounts (`id`, `uuid`, `bank_id`, `owner`, `balance`) " +
						"VALUES (? , ? , ? , ? , ?) ON DUPLICATE KEY UPDATE `owner` = VALUES(`owner`), balance = VALUES(`balance`)")) {
					SQLUtils.setInt(stmt, 1, member.getBankMemberId());
					SQLUtils.setUUID(stmt, 2, member.getPlayerId());
					SQLUtils.setInt(stmt, 3, bank.getId());
					SQLUtils.setBoolean(stmt, 4, member.isBankOwner());
					SQLUtils.setDecimal(stmt, 5, member.getBalance());
					stmt.executeUpdate();
				}
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
	}

	public @Nullable
	String getBankName(int bankId) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT `name` FROM ae_banks WHERE bank_id = ?")) {
				SQLUtils.setInt(stmt, 1, bankId);
				try (ResultSet resultSet = stmt.executeQuery()) {
					if (resultSet.next()) {
						return SQLUtils.getString(resultSet, "name");
					}
				}
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
		return null;
	}

	public @Nullable
	Integer getBankId(@Nonnull String name) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT `bank_id` FROM ae_banks WHERE `name` = ?")) {
				SQLUtils.setString(stmt, 1, name);
				try (ResultSet resultSet = stmt.executeQuery()) {
					if (resultSet.next()) {
						return SQLUtils.getInt(resultSet, "id");
					}
				}
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
		return null;
	}

	public @Nullable
	UserAccount getUserAccount(@Nonnull UUID playerId) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ae_accounts WHERE uuid = ?")) {
				SQLUtils.setUUID(stmt, 1, playerId);
				try (ResultSet resultSet = stmt.executeQuery()) {
					if (!resultSet.next()) {
						return null;
					}
					BigDecimal balance = SQLUtils.getBigDecimal(resultSet, "balance");
					Boolean infinite = SQLUtils.getBoolean(resultSet, "infinite");
					return new DefaultUserAccount(playerId, balance, infinite == null ? false : infinite);
				}
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
		return null;
	}

	public void updateSaveUser(UserAccount account) {
		try (Connection connection = MySQL.getConnection()) {
			try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO ae_accounts (`uuid`, `balance`, `infinite`) " +
					"VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE balance = VALUES(`balance`), infinite = VALUES(`infinite`)")) {

				SQLUtils.setUUID(stmt, 1, account.getPlayerId());
				SQLUtils.setDecimal(stmt, 2, account.getBalanceIgnoreInfinite());
				SQLUtils.setBoolean(stmt, 3, account.isInfinite());

				stmt.executeUpdate();
			}
		} catch (SQLException exc) {
			onDBError(exc);
		}
	}

}
