package me.petomka.asyncecon.core.communication;

import lombok.Getter;
import me.petomka.asyncecon.core.communication.packets.AEPacket;
import net.techcable.event4j.EventBus;

public class AEPacketHandler {

	public static final String PLUGIN_CHANNEL_NAME = "SNMPacketChannel";

	@Getter
	private static AEPacketHandler instance = new AEPacketHandler();

	private EventBus<Object, Object> eventBus;

	public AEPacketHandler() {
		eventBus = EventBus.builder().executorFactory(AEPacketEventExecutor.PACKET_EVENT_LISTENER_FACTORY)
				.eventMarker(method -> method.isAnnotationPresent(AEPacketEventHandler.class) ?
						method.getAnnotation(AEPacketEventHandler.class).priority()::ordinal : null)
				.build();

	}

	public void registerListener(AEPacketListener listener) {
		eventBus.register(listener);
	}

	public void callPacketReceiveEvent(AEPacket packet) {
		eventBus.fire(packet);
	}

}
