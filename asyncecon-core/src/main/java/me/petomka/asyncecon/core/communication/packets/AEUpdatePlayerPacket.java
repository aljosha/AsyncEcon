package me.petomka.asyncecon.core.communication.packets;

import lombok.Getter;

import javax.annotation.Nonnull;
import java.util.UUID;

public class AEUpdatePlayerPacket extends AEPacket {

	@Getter
	final UUID playerToUpdateUUID;

	public AEUpdatePlayerPacket(@Nonnull UUID senderUUID, @Nonnull UUID receiverUUID, @Nonnull UUID playerToUpdateUUID) {
		super(senderUUID, receiverUUID);
		this.playerToUpdateUUID = playerToUpdateUUID;
	}
}
