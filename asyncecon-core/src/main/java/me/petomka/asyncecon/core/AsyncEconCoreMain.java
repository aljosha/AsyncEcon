package me.petomka.asyncecon.core;

import com.google.common.collect.Lists;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import me.petomka.asyncecon.api.AsyncEcon;
import me.petomka.asyncecon.api.IDatabaseCache;
import me.petomka.asyncecon.api.economy.Bank;
import me.petomka.asyncecon.api.economy.BankMember;
import me.petomka.asyncecon.api.economy.UserAccount;
import me.petomka.asyncecon.core.commands.AsyncEconCommand;
import me.petomka.asyncecon.core.common.CommonLogger;
import me.petomka.asyncecon.core.common.Defaults;
import me.petomka.asyncecon.core.communication.AEPacketHandler;
import me.petomka.asyncecon.core.communication.ByteArrayUtils;
import me.petomka.asyncecon.core.communication.packets.AEPacket;
import me.petomka.asyncecon.core.communication.packets.AEUpdatePlayerPacket;
import me.petomka.asyncecon.core.database.AsyncSaveTask;
import me.petomka.asyncecon.core.database.DatabaseCache;
import me.petomka.asyncecon.core.database.DatabaseModel;
import me.petomka.asyncecon.core.database.MySQL;
import me.petomka.asyncecon.core.database.SaveListener;
import me.petomka.asyncecon.core.economy.DefaultUserAccount;
import me.petomka.asyncecon.core.stuff.PacketListener;
import me.petomka.asyncecon.core.util.ConfigUtils;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Getter
public class AsyncEconCoreMain extends JavaPlugin implements AsyncEcon, PluginMessageListener {

	@Getter
	private static AsyncEconCoreMain instance;

	//DB Variables
	private String dbHost;
	private int dbPort;
	private String dbUsername;
	private String dbPassword;
	private String dbDatabasename;
	private boolean dbSSL;

	//Currency variables
	private Pattern currencyCaptureRegex;
	private String currencyFormat;

	//Stuff
	private int saveTaskInterval;

	private boolean economyRegistered = false;
	private boolean databaseConnected = false;

	@Getter
	private boolean bungeeSupport = false;

	@Override
	public void onLoad() {
		instance = this;
		initConfig();
		try {
			MySQL.init();
			databaseConnected = true;
		} catch (Exception e) {
			getLogger().log(Level.SEVERE, "Database connection could not be established", e);
			return;
		}
		if (registerEconomy()) {
			economyRegistered = true;
		}
	}

	@Override
	public void onEnable() {
		if (!economyRegistered || !databaseConnected) {
			panic("Vault economy could not be initialized!");
			return;
		}

		CommonLogger.setLogger(getLogger());

		initCommands();
		initListeners();
		initTasks();

		DatabaseModel.getInstance().createTables();
		DatabaseCache.getInstance().loadBanks();

		if (bungeeSupport) {
			Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
			Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
			AEPacketHandler.getInstance().registerListener(new PacketListener());
		}

		instance = this;
	}

	public static void updateBungeeUser(UUID playerToUpdateUUID) {
		DatabaseCache.getInstance().saveUser(playerToUpdateUUID);
		Player player = Bukkit.getOnlinePlayers().stream().findAny().orElse(null);
		if (player == null) {
			AsyncEconCoreMain.getInstance().getLogger().log(Level.WARNING, "Tried to send plugin message via " +
					"non existent player, server is empty.");
			return;
		}
		AEUpdatePlayerPacket packet = new AEUpdatePlayerPacket(playerToUpdateUUID, playerToUpdateUUID, playerToUpdateUUID);
		sendPacket(player, packet);
	}

	public static void updateBungeeUser(UUID senderUUID, UUID playerToUpdateUUID) {
		DatabaseCache.getInstance().saveUser(playerToUpdateUUID);
		Player player = Bukkit.getPlayer(senderUUID);
		if (player == null) {
			AsyncEconCoreMain.getInstance().getLogger().log(Level.WARNING, "Tried to send plugin message via " +
					"non existent player, UUID was: " + senderUUID);
			return;
		}
		AEUpdatePlayerPacket packet = new AEUpdatePlayerPacket(senderUUID, playerToUpdateUUID, playerToUpdateUUID);
		sendPacket(player, packet);
	}

	public static void sendPacket(Player sender, AEPacket packet) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Forward");
		out.writeUTF("ALL");
		out.writeUTF(Defaults.PLUGIN_CHANNEL_NAME);

		byte[] object = packet.toByteArray();
		out.writeShort(object.length);
		out.write(object);
		Bukkit.getScheduler().callSyncMethod(AsyncEconCoreMain.getInstance(), () -> {
			sender.sendPluginMessage(AsyncEconCoreMain.getInstance(), "BungeeCord", out.toByteArray());
			return null;
		});
	}

	@Override
	public void onPluginMessageReceived(String s, Player player, byte[] bytes) {
		ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
		if (!in.readUTF().equals(Defaults.PLUGIN_CHANNEL_NAME)) {
			return;
		}
		short len = in.readShort();
		byte[] byteObject = new byte[len];
		in.readFully(byteObject);
		Object object = ByteArrayUtils.toObject(byteObject);
		if (object instanceof AEPacket) {
			AEPacketHandler.getInstance().callPacketReceiveEvent((AEPacket) object);
		} else {
			getLogger().log(Level.WARNING, "Received unknown object in packet channel: " + object);
		}
	}

	public void panic(String message) {
		panic(message, null);
	}

	public void panic(String message, @Nullable Throwable exception) {
		if (exception == null) {
			getLogger().log(Level.SEVERE, message);
		} else {
			getLogger().log(Level.SEVERE, message, exception);
		}
		getServer().getPluginManager().disablePlugin(this);
	}

	@Override
	public void onDisable() {
		if (databaseConnected) {
			DatabaseCache.getInstance().saveAll();
			MySQL.closeDatasource();
		}
	}

	@Override
	public void reloadConfig() {
		super.reloadConfig();
		ConfigUtils.setConfig(getConfig());
	}

	private void initConfig() {
		FileConfiguration config = getConfig();
		ConfigUtils.setConfig(config);

		/*
		 * General stuff
		 * */
		config.addDefault("general.save-task-interval-ticks", 6000);
		config.addDefault("general.bungee-support", false);
		config.addDefault("general.do-logging", false);

		saveTaskInterval = config.getInt("general.save-task-interval-ticks");
		bungeeSupport = config.getBoolean("general.bungee-support");

		/*
		 * DB Stuff
		 * */
		config.addDefault("db.host", "localhost");
		config.addDefault("db.port", 3306);
		config.addDefault("db.username", "root");
		config.addDefault("db.password", "ver1S3cur3!");
		config.addDefault("db.dbname", "myEpicDatabase");
		config.addDefault("db.ssl", false);

		dbHost = config.getString("db.host");
		dbPort = config.getInt("db.port");
		dbUsername = config.getString("db.username");
		dbPassword = config.getString("db.password");
		dbDatabasename = config.getString("db.dbname");
		dbSSL = config.getBoolean("db.ssl");

		/*
		 * Currency stuff
		 * */
		config.addDefault("currency.singular", "Dollar");
		config.addDefault("currency.plural", "Dollars");
		config.addDefault("currency.capture", "(?<dollar>[\\d,]*)\\.(?<cents>\\d*)");
		config.addDefault("currency.format", "%1$s$ %2$sct");

		currencyCaptureRegex = Pattern.compile(config.getString("currency.capture"));
		currencyFormat = config.getString("currency.format");

		/*
		 * Message stuff
		 * */
		config.addDefault("message.prefix", "&9&lAE &8&l> ");
		config.addDefault("message.unknown-player", "<prefix>&cUnknown player!");
		config.addDefault("message.cant-afford", "<prefix>&cYou don't have enough money.");
		config.addDefault("message.no-number", "<prefix>&cYou did not pass a valid number.");
		config.addDefault("message.no-permission", "&cInsufficient permissions.");
		config.addDefault("message.no-permissions", "&cYou cannot issue any action.");
		config.addDefault("message.players-only", "This command can only be used by players!");

		config.addDefault("message.account.head", "&9Your account:");
		config.addDefault("message.account.balance", "&aBalance: &e<balance>");
		config.addDefault("message.account.balance-hover", "&7Your balance.");
		config.addDefault("message.account.infinite-true", "&aAccount is infinite.");
		config.addDefault("message.account.infinite-true-hover", "&7You can never run out of money! Happy shopping!");
		config.addDefault("message.account.infinite-false", "&aAccount is finite.");
		config.addDefault("message.account.infinite-false-hover", "&7Watch your finances!");
		config.addDefault("message.account.balance-real", "&aReal Balance: &e<balance>");
		config.addDefault("message.account.balance-real-hover", "&7Real balance without considering infinite money");
		config.addDefault("message.account.send", "&aSend money to another player");
		config.addDefault("message.account.send-hover", "&7/money send <player> <amount>");
		config.addDefault("message.account.send-click", "/money send ");
		config.addDefault("message.account.bank", "&aSee your bank overview");
		config.addDefault("message.account.bank-hover", "&7/money banks");
		config.addDefault("message.account.bank-click", "/money banks ");
		config.addDefault("message.account.info", "&aLookup someone else's account");
		config.addDefault("message.account.info-hover", "&7/money info <player>");
		config.addDefault("message.account.info-click", "/money info ");
		config.addDefault("message.account.give", "&aAdd money to someone's account out of thin air");
		config.addDefault("message.account.give-hover", "&7/money give <player> <amount>");
		config.addDefault("message.account.give-click", "/money give ");
		config.addDefault("message.account.take", "&aTake money from someone's account");
		config.addDefault("message.account.take-hover", "&7/money take <player> <amount>");
		config.addDefault("message.account.take-click", "/money take ");
		config.addDefault("message.account.set", "&aSet another player's balance");
		config.addDefault("message.account.set-hover", "&7/money set <player> <amount>");
		config.addDefault("message.account.set-click", "/money set ");
		config.addDefault("message.account.infinite", "&aSet an account to infinite");
		config.addDefault("message.account.infinite-hover", "&7/money infinite <player>");
		config.addDefault("message.account.infinite-click", "/money infinite ");
		config.addDefault("message.account.db-upload", "&aUpload data to database");
		config.addDefault("message.account.db-upload-hover", "&7/money db upload <player>|@<bankname>|*|@*");
		config.addDefault("message.account.db-upload-click", "/money db upload ");
		config.addDefault("message.account.db-download", "&aDownload data from database");
		config.addDefault("message.account.db-download-hover", "&7/money db download <player>|@<bankname>|*|@*");
		config.addDefault("message.account.db-download-click", "/money db download ");
		config.addDefault("message.account.reload-config", "&aReload configuration");
		config.addDefault("message.account.reload-config-hover", "&7/money reload");
		config.addDefault("message.account.reload-config-click", "/money reload");

		config.addDefault("message.account-other.head", "&e<player>&9's account:");
		config.addDefault("message.account-other.no-permissions", "&cYou cannot issue any action.");
		config.addDefault("message.account-other.balance", "&aBalance: &e<balance>");
		config.addDefault("message.account-other.send", "&aSend money to <player>");
		config.addDefault("message.account-other.bank", "&aSee <player>'s bank overview");
		config.addDefault("message.account-other.give", "&aGive <player> money");
		config.addDefault("message.account-other.take", "&aTake money from <player>");
		config.addDefault("message.account-other.set", "&aSet <player>'s money");
		config.addDefault("message.account-other.infinite", "&aGrant <player> infinite money");

		config.addDefault("message.send.sent", "<prefix>&bYou sent &e<balance> &bto &e<receiver>");
		config.addDefault("message.send.received", "<prefix>&bYou received &e<balance> &bfrom &e<sender>");
		config.addDefault("message.send.self-sent", "<prefix>&cYou can't send money to yourself.");

		config.addDefault("message.banks.head", "&9Banks you are member of:");
		config.addDefault("message.banks.no-banks", "&cYou are not member of any bank.");
		config.addDefault("message.banks.bank", "&a<bankname>: &e<balance>");
		config.addDefault("message.banks.bank-hover", "&7/money bank <bankname>");
		config.addDefault("message.banks.bank-click", "/money bank <bankname>");

		config.addDefault("message.give.given", "<prefix>&bYou gave &e<balance> &bto &e<receiver>");
		config.addDefault("message.give.received", "<prefix>&bYou received &e<balance> &bfrom &e<sender>");

		config.addDefault("message.take.taken", "<prefix>&bYou took &e<balance> &bfrom &e<player>");
		config.addDefault("message.take.withdrawn", "<prefix>&e<player> &bhas taken &e<balance> &bfrom your account.");

		config.addDefault("message.set.set-other", "<prefix>&bYou set &e<player>&b's balance to &e<balance>");
		config.addDefault("message.set.got-set", "<prefix>&bYour balance was set to &e<balance> &bby &e<player>");

		config.addDefault("message.infinite.now-infinite", "<prefix>&bYour account is now infinite.");
		config.addDefault("message.infinite.no-longer-infinite", "<prefix>&bYour account is no longer infinite.");
		config.addDefault("message.infinite.set-finite", "<prefix>&bYou set &e<player>&b's account to finite.");
		config.addDefault("message.infinite.set-infinite", "<prefix>&bYou set &e<player>&b's account to infinite.");

		config.addDefault("message.db.illegal-selector", "<prefix>&cIllegal target selector.");

		config.addDefault("message.db.upload.all-users", "<prefix>&bYou uploaded all cached user accounts to database.");
		config.addDefault("message.db.upload.user", "<prefix>&bYou uploaded &e<player>&b's user account to database.");
		config.addDefault("message.db.upload.all-banks", "<prefix>&bYou uploaded all bank data to database.");
		config.addDefault("message.db.upload.bank", "<prefix>&bYou uploaded data of bank &e<bankname> &bto database.");

		config.addDefault("message.db.download.all-users", "<prefix>&bYou cleared the user cache - data will be redownloaded from database.");
		config.addDefault("message.db.download.user", "<prefix>&bYou cleared &e<player>&b's user account from cache - data will be redownloaded.");
		config.addDefault("message.db.download.all-banks", "<prefix>&bYou redownloaded all bank user accounts from database.");
		config.addDefault("message.db.download.bank", "<prefix>&bYou redownloaded user accounts of bank &e<bankname> &bfrom database.");

		config.addDefault("message.config.reloaded", "<prefix>&bConfiguration reloaded.");

		config.options().copyDefaults(true);
		saveConfig();
	}

	private void initCommands() {
		getCommand("asyncecon").setExecutor(new AsyncEconCommand());
	}

	private void initTasks() {
		new AsyncSaveTask(saveTaskInterval, this);
	}

	private void initListeners() {
		Bukkit.getServer().getPluginManager().registerEvents(new SaveListener(), this);
	}

	private boolean registerEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		try {
			getServer().getServicesManager().register(Economy.class, this, this, ServicePriority.High);
		} catch (Exception exc) {
			getLogger().log(Level.SEVERE, "Could not register Economy.", exc);
			return false;
		}
		return true;
	}

	public static void updatePlayerOnBungeeSupport(UUID playerUUID) {
		if (AsyncEconCoreMain.getInstance().bungeeSupport && Bukkit.getPlayer(playerUUID) == null) {
			updateBungeeUser(playerUUID);
		}
	}

	public static void async(Runnable runnable) {
		Bukkit.getScheduler().runTaskAsynchronously(AsyncEconCoreMain.getInstance(), runnable);
	}

	@Override
	public List<Bank> getAsyncEconBanks() {
		return DatabaseCache.getInstance().getLoadedBanks();
	}

	@Override
	public UserAccount getUser(UUID playerId) {
		return DatabaseCache.getInstance().getUser(playerId);
	}

	@Override
	public UserAccount getConsoleAccount() {
		return DefaultUserAccount.getConsoleAccount();
	}

	@Override
	public IDatabaseCache getDatabaseCache() {
		return DatabaseCache.getInstance();
	}

	@Override
	public boolean hasBankSupport() {
		return false;
	}

	@Override
	public int fractionalDigits() {
		return 2;
	}

	private DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
	private DecimalFormat decimalFormat = new DecimalFormat("#,##0.00", symbols);

	@Override
	public String format(double v) {
		String s = decimalFormat.format(v);
		Matcher matcher = currencyCaptureRegex.matcher(s);
		List<String> groups = Lists.newArrayList();
		for (int i = 1; i <= matcher.groupCount(); i++) {
			if (!matcher.matches()) {
				continue;
			}
			groups.add(matcher.group(i));
		}
		return String.format(currencyFormat, groups.toArray());
	}

	@Override
	public String currencyNamePlural() {
		return getConfig().getString("currency.singular");
	}

	@Override
	public String currencyNameSingular() {
		return getConfig().getString("currency.plural");
	}

	@Override
	public boolean hasAccount(String s) {
		return true;
	}

	@Override
	public boolean hasAccount(OfflinePlayer offlinePlayer) {
		return true;
	}

	@Override
	public boolean hasAccount(String s, String s1) {
		OfflinePlayer player = getServer().getOfflinePlayer(s);
		if (player == null) {
			return false;
		}
		return hasAccount(player, s1);
	}

	@Override
	public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
		return true;
	}

	@Override
	public double getBalance(String s) {
		Player player = getServer().getPlayer(s);
		if (player == null) {
			return 0;
		}
		return getBalance(player);
	}

	@Override
	public double getBalance(OfflinePlayer offlinePlayer) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		return DatabaseCache.getInstance().getUser(offlinePlayer.getUniqueId()).getBalance().doubleValue();
	}

	@Override
	public double getBalance(String s, String s1) {
		Player player = getServer().getPlayer(s);
		if (player == null) {
			return 0;
		}
		return getBalance(player, s1);
	}

	@Override
	public double getBalance(OfflinePlayer offlinePlayer, String s) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		return getBalance(offlinePlayer);
	}

	@Override
	public boolean has(String s, double v) {
		Player player = getServer().getPlayer(s);
		if (player == null) {
			return false;
		}
		return has(player, v);
	}

	@Override
	public boolean has(OfflinePlayer offlinePlayer, double v) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		UserAccount account = getUser(offlinePlayer.getUniqueId());
		if (account.isInfinite()) {
			return true;
		}
		return account.getBalance().doubleValue() >= v;
	}

	@Override
	public boolean has(String s, String s1, double v) {
		OfflinePlayer player = getServer().getOfflinePlayer(s);
		if (player == null) {
			return false;
		}
		return has(player, s1, v);
	}

	@Override
	public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		return has(offlinePlayer, v);
	}

	private EconomyResponse econResponse(int amount, double balance, EconomyResponse.ResponseType type, String msg) {
		return new EconomyResponse(amount, balance, type, msg);
	}

	private EconomyResponse unknownPlayerEconResponse() {
		return econResponse(1, 0, EconomyResponse.ResponseType.FAILURE, "Unknown player");
	}

	private EconomyResponse unknownBankEconResponse() {
		return econResponse(1, 0, EconomyResponse.ResponseType.FAILURE, "Unknown bank");
	}

	private EconomyResponse playerNotMemberOfBankEconResponse() {
		return econResponse(1, 0, EconomyResponse.ResponseType.FAILURE, "Player is not a member of bank");
	}

	private EconomyResponse successEconResponse(BigDecimal balance) {
		return econResponse(1, balance.doubleValue(), EconomyResponse.ResponseType.SUCCESS, "");
	}

	private EconomyResponse bankAlreadyExistsEconResponse() {
		return econResponse(1, 0, EconomyResponse.ResponseType.FAILURE, "Bank with this name already exists");
	}

	private EconomyResponse notImplemented() {
		return econResponse(1, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "This feature is not supported");
	}

	private EconomyResponse negativeResponse() {
		return econResponse(1, 0, EconomyResponse.ResponseType.FAILURE, "No");
	}

	@Override
	public EconomyResponse withdrawPlayer(String s, double v) {
		OfflinePlayer player = getServer().getOfflinePlayer(s);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return withdrawPlayer(player, v);
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double v) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		UserAccount account = getUser(offlinePlayer.getUniqueId());
		account.setBalance(account.getBalance().subtract(new BigDecimal(v)));
		return successEconResponse(account.getBalance());
	}

	@Override
	public EconomyResponse withdrawPlayer(String s, String s1, double v) {
		OfflinePlayer player = getServer().getOfflinePlayer(s);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return withdrawPlayer(player, s1, v);
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, String s, double v) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		return withdrawPlayer(offlinePlayer, v);
	}

	@Override
	public EconomyResponse depositPlayer(String s, double v) {
		OfflinePlayer player = getServer().getOfflinePlayer(s);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return depositPlayer(player, v);
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double v) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		UserAccount account = getUser(offlinePlayer.getUniqueId());
		account.setBalance(account.getBalance().add(new BigDecimal(v)));
		return successEconResponse(account.getBalance());
	}

	@Override
	public EconomyResponse depositPlayer(String s, String s1, double v) {
		OfflinePlayer player = getServer().getOfflinePlayer(s);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return depositPlayer(player, s1, v);
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, String s, double v) {
		updatePlayerOnBungeeSupport(offlinePlayer.getUniqueId());
		return depositPlayer(offlinePlayer, v);
	}

	@Override
	public EconomyResponse createBank(String s, String s1) {
		OfflinePlayer player = getServer().getOfflinePlayer(s1);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return createBank(s, player);
	}

	@Override
	public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
		if (!DatabaseCache.getInstance().createBank(s)) {
			return bankAlreadyExistsEconResponse();
		}
		Bank bank = DatabaseCache.getInstance().getBank(s);
		if (bank == null) {
			throw new IllegalStateException("Bank that was just created doesn't exist! Name: " + s);
		}

		bank.addMember(offlinePlayer.getUniqueId(), true, new BigDecimal(0));
		return successEconResponse(new BigDecimal(0));
	}

	@Override
	public EconomyResponse deleteBank(String s) {
		Bank bank = DatabaseCache.getInstance().getBank(s);
		if (bank == null) {
			return unknownBankEconResponse();
		}
		if (!DatabaseCache.getInstance().deleteBank(bank.getId())) {
			return econResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "Unknown error. Database?");
		}
		return successEconResponse(bank.getBalance());
	}

	@Override
	public EconomyResponse bankBalance(String s) {
		Bank bank = DatabaseCache.getInstance().getBank(s);
		if (bank == null) {
			return unknownBankEconResponse();
		}
		return successEconResponse(bank.getBalance());
	}

	@Override
	public EconomyResponse bankHas(String s, double v) {
		Bank bank = DatabaseCache.getInstance().getBank(s);
		if (bank == null) {
			return unknownBankEconResponse();
		}
		return successEconResponse(bank.getBalance());
	}

	@Override
	public EconomyResponse bankWithdraw(String s, double v) {
		return notImplemented();
	}

	@Override
	public EconomyResponse bankDeposit(String s, double v) {
		return notImplemented();
	}

	@Override
	public EconomyResponse isBankOwner(String s, String s1) {
		OfflinePlayer player = getServer().getOfflinePlayer(s1);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return isBankOwner(s, player);
	}

	@Override
	public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
		Bank bank = DatabaseCache.getInstance().getBank(s);
		if (bank == null) {
			return unknownBankEconResponse();
		}
		BankMember member = bank.getMember(offlinePlayer.getUniqueId());
		if (member == null) {
			return playerNotMemberOfBankEconResponse();
		}
		if (member.isBankOwner()) {
			return successEconResponse(member.getBalance());
		}
		return negativeResponse();
	}

	@Override
	public EconomyResponse isBankMember(String s, String s1) {
		OfflinePlayer player = getServer().getOfflinePlayer(s1);
		if (player == null) {
			return unknownPlayerEconResponse();
		}
		return isBankMember(s, player);
	}

	@Override
	public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
		Bank bank = DatabaseCache.getInstance().getBank(s);
		if (bank == null) {
			return unknownBankEconResponse();
		}
		BankMember member = bank.getMember(offlinePlayer.getUniqueId());
		if (member == null) {
			return playerNotMemberOfBankEconResponse();
		}
		return successEconResponse(member.getBalance());
	}

	@Override
	public List<String> getBanks() {
		return getAsyncEconBanks().stream().map(Bank::getName).collect(Collectors.toList());
	}

	@Override
	public boolean createPlayerAccount(String s) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(String s, String s1) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
		return true;
	}
}
