package me.petomka.asyncecon.core.economy;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import me.petomka.asyncecon.api.economy.UserAccount;
import org.bukkit.Bukkit;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"playerId"})
public class DefaultUserAccount implements UserAccount {

	public static UserAccount getNewAccount(UUID playerId) {
		return new DefaultUserAccount(playerId, new BigDecimal(0, MathContext.DECIMAL64),
				false);
	}

	@Getter
	private static UserAccount consoleAccount = new DefaultUserAccount(new UUID(0, 0),
			new BigDecimal(0), true);

	private final UUID playerId;
	private BigDecimal balance;
	private boolean infinite;

	public BigDecimal getBalance() {
		if (infinite) {
			return new BigDecimal(999_999_999_999D);
		}
		return getBalanceIgnoreInfinite();
	}

	@Override
	public BigDecimal getBalanceIgnoreInfinite() {
		if (balance.doubleValue() < 0) {
			return new BigDecimal(0);
		}
		return balance;
	}

	@Override
	public boolean canAfford(double v) {
		return infinite || balance.doubleValue() >= v;
	}

	@Override
	public void deposit(double amount) {
		balance = balance.add(new BigDecimal(amount)).setScale(2, RoundingMode.HALF_DOWN);
	}

	@Override
	public void withdraw(double amount) {
		balance = balance.subtract(new BigDecimal(amount)).setScale(2, RoundingMode.HALF_DOWN);
		if (balance.doubleValue() < 0) {
			balance = new BigDecimal(0).setScale(2, RoundingMode.HALF_DOWN);
		}
	}

	@Override
	public boolean isOnline() {
		return Bukkit.getPlayer(playerId) != null;
	}


}
