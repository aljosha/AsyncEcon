package me.petomka.asyncecon.core.database;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

@UtilityClass
public class SQLUtils {

	public static void setString(PreparedStatement stmt, int paramIndex, String string) throws SQLException {
		if (string == null) {
			stmt.setNull(paramIndex, Types.VARCHAR);
		} else {
			stmt.setString(paramIndex, string);
		}
	}

	public static String getString(ResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getString(columnLabel);
	}

	public static void setDecimal(PreparedStatement stmt, int paramIndex, BigDecimal decimal) throws SQLException {
		if (decimal == null) {
			stmt.setNull(paramIndex, Types.DECIMAL);
			return;
		}
		stmt.setBigDecimal(paramIndex, decimal);
	}

	public static BigDecimal getBigDecimal(ResultSet resultSet, String columnLabel) throws SQLException {
		return resultSet.getBigDecimal(columnLabel);
	}

	public static void setInt(PreparedStatement stmt, int paramIndex, Integer integer) throws SQLException {
		if (integer == null) {
			stmt.setNull(paramIndex, Types.INTEGER);
			return;
		}
		stmt.setInt(paramIndex, integer);
	}

	public Integer getInt(ResultSet resultSet, String columnLabel) throws SQLException {
		int result = resultSet.getInt(columnLabel);
		if (resultSet.wasNull()) {
			return null;
		}
		return result;
	}

	public static void setBoolean(PreparedStatement stmt, int paramIndex, Boolean value) throws SQLException {
		if (value == null) {
			stmt.setNull(paramIndex, Types.BOOLEAN);
			return;
		}
		stmt.setBoolean(paramIndex, value);
	}

	public Boolean getBoolean(ResultSet resultSet, String columnLabel) throws SQLException {
		boolean value = resultSet.getBoolean(columnLabel);
		if(resultSet.wasNull()) {
			return null;
		}
		return value;
	}

	public static void setUUID(PreparedStatement stmt, int paramIndex, UUID uuid) throws SQLException {
		if (uuid == null) {
			stmt.setNull(paramIndex, Types.VARCHAR);
			return;
		}
		stmt.setString(paramIndex, uuid.toString());
	}

	public static UUID getUUID(ResultSet resultSet, String columnLabel) throws SQLException {
		return UUID.fromString(getString(resultSet, columnLabel));
	}

}
