package me.petomka.asyncecon.core.commands;

import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class CommandUtils {

	public static @Nonnull
	List<String> copyMatches(String input, List<String> possibilities) {
		List<String> result = Lists.newArrayList();
		possibilities.stream()
				.map(String::toLowerCase)
				.filter(s -> s.startsWith(input.toLowerCase()))
				.forEach(result::add);
		return result;
	}

	public static @Nullable
	Double getDouble(String arg) {
		try {
			return Double.parseDouble(arg);
		} catch (Exception ignored) {
		}
		try {
			return (double) (int) Integer.parseInt(arg);
		} catch (Exception ignored) {
		}
		return null;
	}

	public static boolean getBoolean(String arg) {
		try {
			return Boolean.parseBoolean(arg);
		} catch (Exception ignored) {
		}
		return false;
	}

}
