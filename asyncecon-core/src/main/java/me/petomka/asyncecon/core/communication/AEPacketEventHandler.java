package me.petomka.asyncecon.core.communication;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface AEPacketEventHandler {

	AEPacketPriority priority() default AEPacketPriority.NORMAL;

	boolean ignoreHandled() default true;

}
